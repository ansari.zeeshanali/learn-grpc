import com.google.common.util.concurrent.ListenableFuture;
import com.learn.example.grpc.HelloRequest;
import com.learn.example.grpc.HelloResponse;
import com.learn.example.grpc.HelloServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.concurrent.ExecutionException;

public class Client {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8080)
                .usePlaintext(true)
                .build();

        HelloServiceGrpc.HelloServiceFutureStub stub
                = HelloServiceGrpc.newFutureStub(channel);

        ListenableFuture<HelloResponse> helloResponse = stub.hello(HelloRequest.newBuilder()
                .setFirstName("Baeldung")
                .setLastName("gRPC")
                .build());
        System.out.println("helloResponse = " + helloResponse.get());
        channel.shutdown();

    }
}
