import com.learn.service.Service;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class Server {
    public static void main(String[] args) throws IOException, InterruptedException {
        Service service = new Service();
        io.grpc.Server server = ServerBuilder
                .forPort(8080)
                .addService(service).build();

        server.start();
        server.awaitTermination();
    }
}
