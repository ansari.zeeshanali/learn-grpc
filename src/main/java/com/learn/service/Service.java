package com.learn.service;

import com.learn.example.grpc.HelloRequest;
import com.learn.example.grpc.HelloResponse;
import com.learn.example.grpc.HelloServiceGrpc.HelloServiceImplBase;
import io.grpc.stub.StreamObserver;

public class Service extends HelloServiceImplBase {

    @Override
    public void hello(HelloRequest request,
                      StreamObserver<HelloResponse> responseObserver) {
        HelloResponse.Builder builder = HelloResponse.newBuilder();
        responseObserver.onNext(builder.setGreeting("Hello").build());
        responseObserver.onCompleted();
    }
}
